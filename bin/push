#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
var util = require('util');
var async = require('async');
var mime = require('mime');
var readdir = require('read-dir-files');
var request = require('request');

// env
var env = require('../etc/env.js');
var dest = path.normalize(__dirname + '/../theme');
var api = 'https://' + env.SHOP_KEY + ':' + env.SHOP_PASS + '@' + env.SHOP_HOST;

// whitelist
var args = process.argv.slice(2).join('➔') + '➔';

async.waterfall([
  getThemes,
  getAssets
], function(err) {
  if (err) {
    return console.log(err.message);
  }
  console.log('all done');
});

function getThemes(cb) {
  request(api + '/admin/themes.json', function(err, res) {
    var themes = JSON.parse(res.body).themes;
    var searched = [];
    for (var i=0; i<themes.length; i++) {
      var theme = themes[i];
      if (theme.name === env.THEME) {
        return cb(null, theme.id);
      }
      searched.push('"' + theme.name + '"');
    }
    cb(new Error('theme "' + env.THEME + '" not found in [ ' + searched.join(', ') + ' ]'));
  });
}

function getAssets(theme, cb) {
  readdir.list(__dirname + '/../theme', { recursive: true }, function(err, filenames) {
    var toPush = [];
    for (var i=0; i<filenames.length; i++) {
      var file = filenames[i];
      var filename = file.split('/theme/').slice(-1)[0];
      var stat = fs.statSync(file);
      if (!stat.isDirectory() && filename.search(/\.git/) === -1) {
        if (args.length === 1 || args.search(new RegExp(filename + '➔')) > -1) {
          (function(file) {
            toPush.push(function(cb) {
              putFile(theme, file, cb);
            });
          })(file);
        }
      }
    }
    async.parallel(toPush, cb);
  });
}

function putFile(theme, file, cb) {
  var type = mime.lookup(file);
  var asset = file.split('/theme/').slice(-1)[0];
  var opts = {
    json: {
      asset: {
        key: asset
      }
    }
  };
  
  if (type.search('image') > -1) {
    opts.json.asset.attachment = fs.readFileSync(file, { encoding: 'base64' });
  } else {
    opts.json.asset.value = fs.readFileSync(file, { encoding: 'utf8' });
  }
  
  request.put(api + '/admin/themes/' + theme + '/assets.json', opts, function(err) {
    if (err) {
      console.log('! error uploading file', opts.json.asset.key);
      return cb(err);
    }
    console.log('+ uploaded file', opts.json.asset.key);
    cb();
  });
}
